import React, {useRef, useState } from "react";
import Button from "../Component/Button";
import Timer from "../Component/Timer";

const HomePage = () => {
    const [time, setTime] = useState(0)
    const [isRunning, setIsRunning] = useState(false)
    const intervalRef = useRef(null);

    const handleStop = () => {
        setIsRunning(false)
        clearInterval(intervalRef.current)
    }

    const handleStart = () => {
        setIsRunning(true)
        intervalRef.current = setInterval(() => {
            setTime((prev) => prev + 10)
        }, 10)
    }
    
    const handleReset = () => {
        setTime(0);
        clearInterval(intervalRef.current);
        isRunning && handleStart() 
      };
  return (
    <div className="homepage_wrapper">

    <div className="homepage_timer_wrapper">
    <Timer time={time}/>
    </div>

    <div className="homepage_button_wrapper">
    <Button onClick={handleReset} text="Reset" color="#ffde59" />
    <Button onClick={handleStart} text="Start" color="#7ed957" isRunning={isRunning}/>
    <Button onClick={handleStop} text="Stop" color="#c9e265" />
    </div>
    </div>
  );
};

export default HomePage;
