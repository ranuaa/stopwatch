import React from "react";

const Timer = ({time}) => {
    const hours = Math.floor(time / 3600000);
    const minutes = Math.floor((time % 3600000) / 60000);
    const seconds = Math.floor((time % 60000) / 1000);
  return (
  <div>
    <h1>{hours.toString().padStart(2, "0")} : {minutes.toString().padStart(2, "0")} : {seconds.toString().padStart(2, "0")}</h1>
    <h1>Jam : Menit : Detik</h1>
  </div>
  );
};

export default Timer;
