import '../App.css'

const Button = ({text, color, onClick, isRunning}) => {
    
  return (
  <div>
    <button 
    disabled={isRunning}
    onClick={() =>  onClick()}
    style={{backgroundColor: color}} 
    className="button">
        {text}
    </button>
  </div>
  );
};

export default Button;
